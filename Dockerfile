FROM ubuntu:xenial

RUN apt-get update
RUN apt-get install -yy software-properties-common curl git rsync unzip apt-transport-https build-essential sshfs

RUN LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt -yy upgrade

COPY installed_php installed_php
RUN apt-get install -y $(cat installed_php)
RUN rm installed_php

RUN apt-get install -y php7.1 php7.1-cli php7.1-curl php7.1-ldap php7.1-sqlite3 php7.1-zip composer yarn nodejs php7.1-xml vim gettext-base openssh-client
RUN composer global require hirak/prestissimo
RUN npm install -g newman bower
RUN npm i npm@latest -g
